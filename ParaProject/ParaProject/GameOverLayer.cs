using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using ParaProject.Classes;
using Microsoft.Xna.Framework.GamerServices;
using System.IO.IsolatedStorage;
using System.IO;
using System.Xml;
using System.Xml.Linq;

namespace ParaProject
{
    class GameOverLayer
    {

        Vector2 mScreenSize;

        Texture2D mBackground;
        Vector2 mBackgroundPosition;
        Vector2 mBackgroundOrigin;

        string mTitle;
        SpriteFont mFont;
        Vector2 mTitlePosition;
        Vector2 mTitleOrigin;

        string mName;
        Vector2 mNamePosition;
        Vector2 mNameOrigin;

        bool mShowKeyboard;

        int mFinalScore;

        public Button SaveButton;

        public GameOverLayer( ContentManager _content, Vector2 _screenSize )
        {
            mScreenSize = _screenSize;

            mBackground =  _content.Load<Texture2D>("Resources\\popup");
            mBackgroundPosition = new Vector2( mScreenSize.X / 2, mScreenSize.Y / 2 + 50 );
            mBackgroundOrigin = new Vector2( mBackground.Width / 2, mBackground.Height / 2 );

            mTitle = "Game Over";
            mFont = _content.Load<SpriteFont>("Resources\\loaded");
            mTitlePosition = new Vector2( mScreenSize.X / 2, 200 );
            mTitleOrigin = new Vector2( mFont.MeasureString( mTitle ).X / 2, mFont.MeasureString( mTitle ).Y / 2 );

            mName = " ";
            mNamePosition = new Vector2( mScreenSize.X / 2, 250 );
            mNameOrigin = new Vector2( mFont.MeasureString( mName ).X / 2, mFont.MeasureString( mName ).Y / 2 );

            SaveButton = new Button( _content.Load<Texture2D>("Resources\\save"), new Vector2( mScreenSize.X / 2, mScreenSize.Y - 80 ) );


            mShowKeyboard = true;
            
        }

        public void OnEndShowKeyBoardInput( IAsyncResult _result )
        {
            save( Guide.EndShowKeyboardInput( _result ) );
        }

        public void Update( int _finalScore )
        {
            if( mShowKeyboard )
            {
                mFinalScore = _finalScore;
                Guide.BeginShowKeyboardInput( PlayerIndex.One, "Game Over", "Insert your name: ", "", new AsyncCallback(OnEndShowKeyBoardInput), null );
                mShowKeyboard = false;
            }

            if( mName == null )
                mName = "Bob";

            mNameOrigin = new Vector2( mFont.MeasureString( mName ).X / 2, mFont.MeasureString( mName ).Y / 2 );
            
        }

        public void save( string _playerName )
        {
            
            mName = _playerName;

            List<ScoreData> aScoreData = DataSingleton.Instance.ScoreData;

            ScoreData scoreData = new ScoreData( mName, mFinalScore );

            int i = aScoreData.Count - 1;
    
            while( i > 0 && mFinalScore > aScoreData[ i - 1 ].Score )
            {
                aScoreData[i] = aScoreData[ i - 1 ];

                i--;
            }
            
            aScoreData[i] = scoreData;

            XDocument doc = XDocument.Parse("<scores />");
                    
            for( int j = 0; j < 5 ; j++ )
            {
                XElement newItem = new XElement("scoredata");
                newItem.Add(XElement.Parse("<username>" + aScoreData[j].Username + "</username>"));
                newItem.Add(XElement.Parse("<score>" + aScoreData[j].Score.ToString() + "</score>"));
                doc.Root.Add(newItem);
            }

            using (IsolatedStorageFile isf = IsolatedStorageFile.GetUserStoreForApplication())
            {
                using (IsolatedStorageFileStream stream = new IsolatedStorageFileStream("Scoreboard.xml", System.IO.FileMode.Create, isf))
                {
                    doc.Save(stream);
                }
            }

        }

        public void Draw( SpriteBatch _spriteBatch )
        {

             _spriteBatch.Draw( mBackground, mBackgroundPosition, null, Color.White, 0f, mBackgroundOrigin, 1, SpriteEffects.None, 1 );

            _spriteBatch.DrawString( mFont, mTitle, mTitlePosition, Color.White, 0f, mTitleOrigin, 1, SpriteEffects.None, 0 );

             _spriteBatch.DrawString( mFont, mName, mNamePosition, Color.White, 0f, mNameOrigin, 1, SpriteEffects.None, 0 );

            SaveButton.Draw( _spriteBatch );
        }

    }
}
