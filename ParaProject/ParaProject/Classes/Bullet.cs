using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace ParaProject.Classes
{
    class Bullet
    {
        Texture2D mSprite;
        Vector2 mPosition;
        Vector2 mOrigin;
    
        Landscape mLandscape;

        public float Angle { get; set; }

        Vector2 mLocalPosition;

        public Bullet( Texture2D _loadedtexture, Landscape _env )
        {
            mSprite = _loadedtexture;
            mLandscape = _env;

            mLocalPosition = new Vector2( 1024, 1024 );
            mPosition = Vector2.Add( mLocalPosition, mLandscape.Position );

            mOrigin = new Vector2 ( mSprite.Width / 2, mSprite.Height / 2 );
        }

        public Vector2 GetWorldPosition()
        {
            return new Vector2( mLocalPosition.X + 1024 / 2 , mLocalPosition.Y + 1024 / 2 );
        }

        public void SetSpritePosition( Vector2 _pos )
        {
            mLocalPosition = _pos;
            mPosition = Vector2.Add( mLocalPosition, mLandscape.Position );
        }

        public Vector2 GetSpritePosition()
        {
            return mLocalPosition;
        }

        public void Draw( SpriteBatch _spriteBatch )
        {
            _spriteBatch.Draw( mSprite, mPosition, null, Color.White, 0f, mOrigin, 1, SpriteEffects.None, 1 );
        }
    }
}
