using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace ParaProject
{

    public class EventData : EventArgs
    {
        public int ScreenId { get; set; }
    }


    class Screen
    {
        
        //The event associated with the Screen. This event is used to raise events
        //back in the main game class to notify the game that something has changed
        //or needs to be changed
        //protected EventHandler ScreenEvent;
        protected EventHandler<EventData> ScreenEvent;
        public Screen( EventHandler<EventData> _screenEvent )
        {
            ScreenEvent = _screenEvent;
        }

        //Update any information specific to the screen
        public virtual void Update( GameTime _gameTime )
        {
        }

        //Draw any objects specific to the screen
        public virtual void Draw( SpriteBatch _spriteBatch )
        {
        }

    }
}
