using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace ParaProject.Classes
{
    class Character
    {

        protected Texture2D sprite;
        protected Vector2 position;
        protected Vector2 origin;
    
        protected Landscape landscape;

        protected Vector2 worldPosition;
    
        protected float rotation;
    
        protected int life;

        public Character()
        {
        }

        public Character( Texture2D _loadedtexture, Vector2 _screenSize )
        {

        }

        public virtual void Draw( SpriteBatch _spriteBatch )
        {

        }

    }
}
