using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ParaProject
{
    public sealed class DataSingleton
    {
        static DataSingleton instance=null;
        static readonly object padlock = new object();

        public List<ScoreData> ScoreData { get; set; }

        DataSingleton()
        {
        }

        public static DataSingleton Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance==null)
                    {
                        instance = new DataSingleton();
                    }
                    return instance;
                }
            }
        }


    }
}
