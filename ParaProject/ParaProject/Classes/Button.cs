using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace ParaProject.Classes
{
    class Button
    {
        Texture2D mSprite;

        Vector2 mPosition;

        Vector2 mOrigin;

        public Button( Texture2D _loadedtexture, Vector2 _position )
        {

            mSprite = _loadedtexture;

            mPosition = _position;

            mOrigin = new Vector2( mSprite.Width / 2, mSprite.Height / 2 );

        }

        public bool IsClicked( Vector2 _location )
        {
            float leftBound = mPosition.X - mSprite.Width / 2;
            float rightBound = mPosition.X + mSprite.Width / 2;
            float topBound = mPosition.Y - mSprite.Height / 2;
            float bottomBound = mPosition.Y + mSprite.Height / 2;

            return ( _location.X > leftBound && _location.X < rightBound && _location.Y > topBound && _location.Y < bottomBound );
        }

        public void Draw( SpriteBatch _spriteBatch )
        {
            _spriteBatch.Draw( mSprite, mPosition, null, Color.White, 0f, mOrigin, 1, SpriteEffects.None, 1 );
        }
    }
}
