using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace ParaProject.Classes
{
    class Landscape
    {

        Texture2D mSprite;

        public Vector2 Position { get; set; }
        Vector2 mOrigin;

        public Landscape( Texture2D _loadedTexture, Vector2 _position )
        {

            mSprite = _loadedTexture;
            mOrigin = new Vector2( mSprite.Width / 2, mSprite.Height / 2 );

            Position = Vector2.Divide( _position, 2 );

        }

        public Vector2 GetSize()
        {
            return new Vector2( mSprite.Width, mSprite.Height );
        }

        public void Draw( SpriteBatch _spriteBatch )
        {
            _spriteBatch.Draw( mSprite, Position, null, Color.White, 0f, mOrigin, 1, SpriteEffects.None, 1 );
        }

    }
}
