using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input.Touch;
using Microsoft.Xna.Framework.Graphics;
using ParaProject.Classes;
using System.IO.IsolatedStorage;
using System.IO;
using System.Xml;
using System.Xml.Linq;

namespace ParaProject
{
    public class ScoreData
    {
        public string Username;
        public int Score;

        public ScoreData()
        {

        }

        public ScoreData( string _username, int _score )
        {
            Username = _username;
            Score = _score;
        }
    }

    class ScoreboardScreen : Screen
    {

        List<ScoreData> mScores;

        SpriteFont mFont;
        Button mMainMenuButton;

        Vector2 mScorePosition;

        public ScoreboardScreen( ContentManager _content, Vector2 _screenSize, EventHandler<EventData> _screenEvent ): base( _screenEvent )
        {
            
            mFont = _content.Load<SpriteFont>("Resources\\loaded");
            mScorePosition = new Vector2( _screenSize.X / 2, 60 );

            mMainMenuButton = new Button( _content.Load<Texture2D>( "Resources\\quit" ), new Vector2( _screenSize.X / 2, _screenSize.Y - 60 ) );

            mScores = new List<ScoreData>();

            LoadData();

            

        }

        //Update all of the elements that need updating in the Title Screen        
        public override void Update( GameTime _gameTime )
        {
            
            TouchCollection touchCollection = TouchPanel.GetState();

            foreach (TouchLocation tl in touchCollection)
            {
                if( mMainMenuButton.IsClicked( tl.Position ) )
                {
                    EventData eventData = new EventData();
                    eventData.ScreenId = 0;

                    ScreenEvent.Invoke(this, eventData );
                }
            }

            base.Update( _gameTime );
        }

        public void LoadData()
        {
            mScores = DataSingleton.Instance.ScoreData;
        }

        //Draw all of the elements that make up the Title Screen
        public override void Draw( SpriteBatch _spriteBatch )
        {
            
            mMainMenuButton.Draw( _spriteBatch );

            int i = 0;

            foreach( ScoreData scoreDate in mScores )
            {
                _spriteBatch.DrawString( mFont, scoreDate.Username + " - " + scoreDate.Score, new Vector2( mScorePosition.X, mScorePosition.Y + i ), Color.White, 0f, new Vector2( mFont.MeasureString( scoreDate.Username + " - " + scoreDate.Score ).X / 2, mFont.MeasureString( scoreDate.Username + " - " + scoreDate.Score ).Y / 2 ), 1, SpriteEffects.None, 0 );

                i += 40;
            }

            base.Draw( _spriteBatch );
        }
    }
}
