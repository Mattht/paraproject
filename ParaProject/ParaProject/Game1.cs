using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Input.Touch;
using Microsoft.Xna.Framework.Media;
using ParaProject.Classes;
using System.IO.IsolatedStorage;
using System.Xml.Linq;

namespace ParaProject
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        Screen mCurrentScreen;
        MainMenuScreen mMainMenuScreen;
        GameplayScreen mGameplayScreen;
        ScoreboardScreen mScoreboardScreen;

        Vector2 screenSize;


        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            graphics.IsFullScreen = true;
            Content.RootDirectory = "Content";

            graphics.SupportedOrientations = 
            DisplayOrientation.LandscapeLeft; 

            // Frame rate is 30 fps by default for Windows Phone.
            //TargetElapsedTime = TimeSpan.FromTicks(333333); 166667
            TargetElapsedTime = TimeSpan.FromTicks( 166667 );

            // Extend battery life under lock.
            InactiveSleepTime = TimeSpan.FromSeconds(1);

        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here

            XDocument doc;

            using (IsolatedStorageFile isf = IsolatedStorageFile.GetUserStoreForApplication())
            {
                if( isf.FileExists( "Scoreboard.xml" ) )
                {
                    using (IsolatedStorageFileStream stream = new IsolatedStorageFileStream("Scoreboard.xml", System.IO.FileMode.Open, isf))
                    {
                        doc = XDocument.Load(stream);
                    }
                }
                else
                {
                    doc = XDocument.Parse("<scores />");
                    
                    for( int i = 0; i < 5 ; i++ )
                    {
                        XElement newItem = new XElement("scoredata");
                        newItem.Add(XElement.Parse("<username>None</username>"));
                        newItem.Add(XElement.Parse("<score>0</score>"));
                        doc.Root.Add(newItem);
                    }

                    using (IsolatedStorageFileStream stream = new IsolatedStorageFileStream("Scoreboard.xml", System.IO.FileMode.Create, isf))
                    {
                        doc.Save(stream);
                    }
                }
            }

            List<ScoreData> scoreData = new List<ScoreData>();

            foreach ( XElement xe in doc.Descendants("scoredata") )
            {
                scoreData.Add( new ScoreData( xe.Element("username").Value, int.Parse( xe.Element("score").Value ) ) );
            }

            DataSingleton.Instance.ScoreData = scoreData;

            screenSize = new Vector2( graphics.GraphicsDevice.DisplayMode.Height, graphics.GraphicsDevice.DisplayMode.Width );
           
            //Initialize the various screens in the game
            mMainMenuScreen = new MainMenuScreen( this.Content, screenSize, new EventHandler<EventData>( MainMenuScreenEvent ) );
            mGameplayScreen = new GameplayScreen( this.Content, screenSize, new EventHandler<EventData>( GameplayScreenEvent ) );
            mScoreboardScreen = new ScoreboardScreen( this.Content, screenSize, new EventHandler<EventData>( ScoreboardScreenEvent ) );

            mCurrentScreen = mMainMenuScreen;

        }
        
        public void MainMenuScreenEvent(object obj, EventData e)
        {
            switch( e.ScreenId )
            {
                case 0:
                    mGameplayScreen.Load();
                    mCurrentScreen = mGameplayScreen;
                    break;

                case 1:
                    mCurrentScreen = mScoreboardScreen;
                    break;
            }
        }

        public void GameplayScreenEvent(object obj, EventData e)
        {
            mCurrentScreen = mMainMenuScreen;
        }

        public void ScoreboardScreenEvent(object obj, EventData e)
        {
            mCurrentScreen = mMainMenuScreen;
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();

            // TODO: Add your update logic here

            mCurrentScreen.Update(gameTime);

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here
            spriteBatch.Begin();

            mCurrentScreen.Draw(spriteBatch);
            
            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
