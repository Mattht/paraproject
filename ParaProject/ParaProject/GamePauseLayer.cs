using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using ParaProject.Classes;

namespace ParaProject
{
    class GamePauseLayer
    {

        Vector2 mScreenSize;

        Texture2D mBackground;
        Vector2 mBackgroundPosition;
        Vector2 mBackgroundOrigin;

        string mTitle;
        SpriteFont mFont;
        Vector2 mTitlePosition;
        Vector2 mTitleOrigin;

        public Button ResumeButton;
        public Button QuitButton;

        public GamePauseLayer( ContentManager _content, Vector2 _screenSize )
        {
            mScreenSize = _screenSize;

            mBackground =  _content.Load<Texture2D>("Resources\\popup");
            mBackgroundPosition = new Vector2( mScreenSize.X / 2, mScreenSize.Y / 2 + 50 );
            mBackgroundOrigin = new Vector2( mBackground.Width / 2, mBackground.Height / 2 );

            mTitle = "Pause";
            mFont = _content.Load<SpriteFont>("Resources\\loaded");
            mTitlePosition = new Vector2( mScreenSize.X / 2, 200 );
            mTitleOrigin = new Vector2( mFont.MeasureString( mTitle ).X / 2, mFont.MeasureString( mTitle ).Y / 2 );

            ResumeButton = new Button( _content.Load<Texture2D>("Resources\\resume"), new Vector2( mScreenSize.X / 2, mScreenSize.Y - 150 ) );
            QuitButton = new Button( _content.Load<Texture2D>("Resources\\quit"), new Vector2( mScreenSize.X / 2, mScreenSize.Y - 80 ) );

        }


        public void Update()
        {



        }

        public void Draw( SpriteBatch _spriteBatch )
        {

            _spriteBatch.Draw( mBackground, mBackgroundPosition, null, Color.White, 0f, mBackgroundOrigin, 1, SpriteEffects.None, 1 );

            _spriteBatch.DrawString( mFont, mTitle, mTitlePosition, Color.White, 0f, mTitleOrigin, 1, SpriteEffects.None, 0 );

            ResumeButton.Draw( _spriteBatch );
            QuitButton.Draw( _spriteBatch );
        }

    }
}
