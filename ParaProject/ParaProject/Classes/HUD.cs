using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using System.IO.IsolatedStorage;
using System.Xml;
using System.IO;

namespace ParaProject.Classes
{
    class HUD
    {

        Player mTargetPlayer;
    
        SpriteFont mFont;

        Vector2 mScorePosition;
        Vector2 mLifePosition;

        public bool GameOver { get; set; }
        public bool GamePaused { get; set; }

        public HUD( SpriteFont _font, Player _player, Vector2 _screenSize)
        {
            mFont = _font;

            mTargetPlayer = _player;

            mScorePosition = new Vector2( _screenSize.X / 2, 20 );
            mLifePosition = new Vector2( 30, 20 );

            GameOver = false;
            GamePaused = false;
        }

        public void Update()
        {
            if( mTargetPlayer.GetLife() == 0 )
            {
                GameOver = true;
            }
        }

        public void Draw( SpriteBatch _spriteBatch)
        {
            _spriteBatch.DrawString( mFont, "Score: " + mTargetPlayer.GetScore(), mScorePosition, Color.White );
            _spriteBatch.DrawString( mFont, "Life: " + mTargetPlayer.GetLife(), mLifePosition, Color.White );
        }
    }
}
