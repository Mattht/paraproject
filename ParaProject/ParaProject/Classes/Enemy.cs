using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace ParaProject.Classes
{
    class Enemy : Character
    {

        Player mTargetPlayer;
    
        int deathValue;

        Vector2 localPosition;

        Random random;

        int speed;

        public Enemy( Texture2D _loadedtexture, Landscape _env, Vector2 _screenSize, int _seed )
        {
            sprite = _loadedtexture;
            landscape = _env;

            localPosition = new Vector2( _screenSize.X / 4, _screenSize.Y / 4 );
            position = Vector2.Add( localPosition, landscape.Position );

            random = new Random( _seed );

            Spawn();

            origin = new Vector2 ( sprite.Width / 2, sprite.Height / 2 );
        }

        public void Spawn()
        {
            int rand = random.Next( 0, 5 );
    
            int sideX = 0;
            int sideY = 0;
            
            switch ( rand ) {
                case 0:
                    sideX =  - 20;
                    sideY =  random.Next( 0, (int) landscape.GetSize().Y ); 
                    break;
                case 1:
                    sideX = (int) landscape.GetSize().X + 20;
                    sideY = random.Next( 0, (int) landscape.GetSize().Y );
                    break;
                case 2:
                    sideX = random.Next( 0, (int) landscape.GetSize().X );
                    sideY = - 20;
                    break;
                case 3:
                    sideX = random.Next( 0, (int) landscape.GetSize().X );
                    sideY = (int) landscape.GetSize().Y + 20;
                    break;
            }

            position = new Vector2( sideX, sideY );
            localPosition = new Vector2( sideX - (int) landscape.GetSize().X / 2, sideY - landscape.GetSize().Y / 2);
            
            int typeRand = random.Next( 0, 2 ) + 1;
    
            deathValue = 10 * typeRand;
            speed = 1 * typeRand;
        }

        public void LockPlayer( Player _player )
        {
            mTargetPlayer = _player;
        }

        Vector2 GetWorldPosition()
        {
            worldPosition = new Vector2( localPosition.X + 1024 / 2 , localPosition.Y + 1024 / 2 );

            return worldPosition;
        }

        public void UpdatePosition()
        {
            // Enenmy rotation:
            float deltaY = mTargetPlayer.GetWorldPosition().Y - this.GetWorldPosition().Y;
            float deltaX = mTargetPlayer.GetWorldPosition().X - this.GetWorldPosition().X;

            rotation = (float) ( Math.Atan2( deltaY, deltaX ) - Math.PI / 2 );
    
            // Enemy position:
            
            Vector2 direction = Vector2.Subtract( mTargetPlayer.GetWorldPosition(), this.GetWorldPosition() );

	        direction.Normalize();
            
            localPosition = Vector2.Add( localPosition, Vector2.Multiply( direction, speed ) );
            position = Vector2.Add( localPosition, landscape.Position );
            
            //System.Diagnostics.Debug.WriteLine( GetWorldPosition() );

            
            // ------ TEST BULLETS
            
            foreach( Bullet bullet in mTargetPlayer.Bullets )
            {
                float bDistance = Vector2.Distance( bullet.GetWorldPosition(), this.GetWorldPosition() );
        
                if ( bDistance < 20 )
                {
                    mTargetPlayer.IncreaseScore( deathValue );
                    Spawn();
                }
            }

    
            // -------- TEST PLAYER
    
            float pDistance = Vector2.Distance( mTargetPlayer.GetWorldPosition(), this.GetWorldPosition() );
                
            if( pDistance < 25 )
            {
                mTargetPlayer.RemoveLife();
                Spawn();
            }
        }

        
        public override void Draw( SpriteBatch _spriteBatch )
        {

            _spriteBatch.Draw( sprite, position, null, Color.White, rotation, origin, 1, SpriteEffects.None, 1 );

        }

    }
}
