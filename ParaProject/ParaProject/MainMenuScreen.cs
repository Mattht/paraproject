using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input.Touch;
using ParaProject.Classes;

namespace ParaProject
{
    class MainMenuScreen : Screen
    {
        
        Vector2 mScreenSize;

        string mTitle;
        SpriteFont mFont;
        Vector2 mTitlePosition;
        Vector2 mTitleOrigin;

        Button mPlayButton;
        Button mScoreButton;

        public MainMenuScreen( ContentManager _content, Vector2 _screenSize, EventHandler<EventData> _screenEvent ): base( _screenEvent )
        {
            mScreenSize = _screenSize;

            mTitle = "Behind Enemy Lines";
            mFont = _content.Load<SpriteFont>("Resources\\loaded");
            mTitlePosition = new Vector2( mScreenSize.X / 2, 150 );
            mTitleOrigin = new Vector2( mFont.MeasureString( mTitle ).X / 2, mFont.MeasureString( mTitle ).Y / 2 );

            mPlayButton = new Button( _content.Load<Texture2D>("Resources\\play"), new Vector2( mScreenSize.X / 2, mScreenSize.Y - 200 ) );
            mScoreButton = new Button( _content.Load<Texture2D>("Resources\\score"), new Vector2( mScreenSize.X / 2, mScreenSize.Y - 140 ) );

        }

        //Update all of the elements that need updating in the Title Screen        
        public override void Update( GameTime _gameTime )
        {
            
            TouchCollection touchCollection = TouchPanel.GetState();

            foreach (TouchLocation tl in touchCollection)
            {
                if( mPlayButton.IsClicked( tl.Position ) )
                {
                    EventData eventData = new EventData();
                    eventData.ScreenId = 0;

                    ScreenEvent.Invoke(this, eventData );
                }
                if( mScoreButton.IsClicked( tl.Position ) )
                {
                    EventData eventData = new EventData();
                    eventData.ScreenId = 1;

                    ScreenEvent.Invoke(this, eventData );
                }
            }

            base.Update( _gameTime );
        }

        //Draw all of the elements that make up the Title Screen
        public override void Draw( SpriteBatch _spriteBatch )
        {
            
            _spriteBatch.DrawString( mFont, mTitle, mTitlePosition, Color.White, 0f, mTitleOrigin, 1, SpriteEffects.None, 0 );

            mPlayButton.Draw( _spriteBatch );
            mScoreButton.Draw( _spriteBatch );

            base.Draw( _spriteBatch );
        }
    }
}
