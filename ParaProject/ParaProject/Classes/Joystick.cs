using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace ParaProject.Classes
{
    class Joystick
    {
        Texture2D mBgSprite;
        Texture2D mJsSprite;
    
        Vector2 mBgPosition;
        Vector2 mJsPosition;

        Vector2 mBgOrigin;
        Vector2 mJsOrigin;

        public float Offset { get; private set; }
        public float Angle { get; private set; }
        float mOffsetX, mOffsetY;

        bool mLocked;
   
        int mTouchHash;

        public Joystick( Texture2D _bgTexture, Texture2D _jsTexture, Vector2 _position )
        {
            mOffsetX = 0;
            mOffsetY = 0;
            Offset = 0;
        
            mBgSprite = _bgTexture;
            mJsSprite = _jsTexture;

            mBgPosition = _position;
            mJsPosition = _position;

            mBgOrigin = new Vector2( mBgSprite.Width / 2, mBgSprite.Height / 2 );
            mJsOrigin = new Vector2( mJsSprite.Width / 2, mJsSprite.Height / 2 );

            mLocked = true;
        }

        public int GetLinkedHash()
        {
            return mTouchHash;
        }

        public void BeginUpdate( Vector2 _location, int _hash )
        {
            mLocked = false;

            UpdatePosition( _location );
    
            mTouchHash = _hash;
        }

        public void UpdatePosition( Vector2 _location )
        {
            if( !mLocked )
            {
                float distance = (float) Math.Sqrt( Math.Pow( ( mBgPosition.X - _location.X ), 2 ) + Math.Pow( ( mBgPosition.Y - _location.Y ) , 2 ) );
        
                if( distance < 20 )
                {
                    mJsPosition = _location;
                }
                else
                {
                    float multiplier = 20 / distance;
            
                    Vector2 newPosition;
                    
                    newPosition = Vector2.Add( mBgPosition, Vector2.Multiply( Vector2.Subtract( _location, mBgPosition ), multiplier ) );
            
                    mJsPosition = newPosition;
                }
                
                mOffsetX = mJsPosition.X - mBgPosition.X;
                mOffsetY = mJsPosition.Y - mBgPosition.Y;
                Offset = (float) Math.Sqrt( Math.Pow( mOffsetX, 2 ) + Math.Pow( mOffsetY, 2 ) );

                float deltaY = _location.Y - mBgPosition.Y;
                float deltaX = _location.X - mBgPosition.X;
                
                Angle = (float) Math.Atan2( deltaY, deltaX );
            }
        }

        public void ResetPosition()
        {
            mJsPosition = mBgPosition;
            mOffsetX = 0;
            mOffsetY = 0;
            Offset = 0;
            mTouchHash = 0;
            mLocked = true;
        }

        public void Draw( SpriteBatch _spriteBatch )
        {
            _spriteBatch.Draw( mBgSprite, mBgPosition, null, Color.White, 0f, mBgOrigin, 1, SpriteEffects.None, 1 );
            _spriteBatch.Draw( mJsSprite, mJsPosition, null, Color.White, 0f, mJsOrigin, 1, SpriteEffects.None, 1 );
        }
    }
}
