using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using ParaProject.Classes;
using Microsoft.Xna.Framework.Input.Touch;

namespace ParaProject
{
    class GameplayScreen : Screen
    {
        Vector2 mScreenSize;
        ContentManager mContent;

        GamePauseLayer mGamePause;
        GameOverLayer mGameOver;

        HUD mHud;

        Landscape mLandScape;

        Joystick mLJoystick;
        Joystick mRJoystick;

        Texture2D mPauseButton;
        Vector2 mPausePosition;
        Vector2 mPauseOrigin;

        Player mPlayer;

        List<Enemy> mEnemies;
 
        public GameplayScreen( ContentManager _content, Vector2 _screenSize, EventHandler<EventData> _screenEvent ): base( _screenEvent )
        {
            mScreenSize = _screenSize;
            mContent = _content;

            Load();
        }

        public void Load()
        {
            mGamePause = new GamePauseLayer( mContent, mScreenSize );
            mGameOver = new GameOverLayer( mContent, mScreenSize );

            mLandScape = new Landscape( mContent.Load<Texture2D>("Resources\\landscape"), mScreenSize );

            mLJoystick = new Joystick( mContent.Load<Texture2D>("Resources\\bgJoystick"), mContent.Load<Texture2D>("Resources\\jsJoystick"), new Vector2( 60, mScreenSize.Y - 60 ) );
            mRJoystick = new Joystick( mContent.Load<Texture2D>("Resources\\bgJoystick"), mContent.Load<Texture2D>("Resources\\jsJoystick"), new Vector2( mScreenSize.X - 60, mScreenSize.Y - 60 ) );

            mPlayer = new Player( mContent.Load<Texture2D>("Resources\\robot"), mContent.Load<Texture2D>("Resources\\bullet"), mLandScape, mScreenSize );
            mPlayer.LinkJoysticks( mLJoystick, mRJoystick );

            mPauseButton = mContent.Load<Texture2D>("Resources\\pauseig");
            mPausePosition = new Vector2( mScreenSize.X - 20, 20 );
            mPauseOrigin = new Vector2( mPauseButton.Width / 2, mPauseButton.Height / 2 );

            mEnemies = new List<Enemy>();

            for( int i = 0; i < 10; i++ )
            {
                Enemy enemy = new Enemy( mContent.Load<Texture2D>("Resources\\Icon-Small-50"), mLandScape, mScreenSize, i );
                enemy.LockPlayer( mPlayer );

                mEnemies.Add( enemy );
            }

            mHud = new HUD( mContent.Load<SpriteFont>("Resources\\loaded"), mPlayer, mScreenSize );
        }

        public override void Update( GameTime _gameTime )
        {
            TouchCollection touchCollection = TouchPanel.GetState();

            if( !mHud.GameOver && !mHud.GamePaused )
            {
                foreach (TouchLocation tl in touchCollection)
                {
                    if ( tl.State == TouchLocationState.Pressed )
                    {
                        if( tl.Position.X > mScreenSize.X - 40 && tl.Position.Y < 40 )
                        {
                            mHud.GamePaused = true;
                        }

                        if( tl.Position.X <= mScreenSize.X / 2 ) 
                        {
                            mLJoystick.BeginUpdate( tl.Position, tl.Id );
                        }
                        else
                        {
                            mRJoystick.BeginUpdate( tl.Position, tl.Id );
                        }
                    }
                    else if( tl.State == TouchLocationState.Moved )
                    {
                        if( tl.Id == mLJoystick.GetLinkedHash() )
                        {
                            mLJoystick.UpdatePosition( tl.Position );
                        }
                        else if( tl.Id == mRJoystick.GetLinkedHash() )
                        {
                            mRJoystick.UpdatePosition( tl.Position );
                        }
                    }
                    else if( tl.State == TouchLocationState.Released )
                    {
                        if( tl.Id == mLJoystick.GetLinkedHash() )
                        {
                            mLJoystick.ResetPosition();
                        }
                        else if( tl.Id == mRJoystick.GetLinkedHash() )
                        {
                            mRJoystick.ResetPosition();
                        }
                    }
                }
            
                mPlayer.UpdatePosition();

                foreach( Enemy enemy in mEnemies )
                {
                    enemy.UpdatePosition();
                }

                mHud.Update();
            }
            else if( mHud.GamePaused )
            {
                foreach (TouchLocation tl in touchCollection)
                {
                    if ( tl.State == TouchLocationState.Pressed )
                    {
                        if( mGamePause.ResumeButton.IsClicked( tl.Position ) ) 
                        {
                            mHud.GamePaused = false;
                        }
                        else if ( mGamePause.QuitButton.IsClicked( tl.Position ) )
                        {
                            EventData eventData = new EventData();
                            eventData.ScreenId = 0;

                            ScreenEvent.Invoke(this, eventData );
                        }
                    }
                }
            }
            else if( mHud.GameOver )
            {
                mGameOver.Update( mPlayer.GetScore() );

                foreach (TouchLocation tl in touchCollection)
                {
                    if ( tl.State == TouchLocationState.Pressed )
                    {
                        if( mGameOver.SaveButton.IsClicked( tl.Position ) ) 
                        {
                            EventData eventData = new EventData();
                            eventData.ScreenId = 0;

                            ScreenEvent.Invoke(this, eventData );
                        }
                    }
                }

            }
 
            base.Update( _gameTime );
        }

        public override void Draw( SpriteBatch _spriteBatch )
        {
            mLandScape.Draw( _spriteBatch );

            foreach( Enemy enemy in mEnemies )
            {
                enemy.Draw( _spriteBatch );
            }

            mPlayer.Draw( _spriteBatch );

            mLJoystick.Draw( _spriteBatch );
            mRJoystick.Draw( _spriteBatch );

            mHud.Draw( _spriteBatch );

            if( mHud.GamePaused )
            {
                mGamePause.Draw( _spriteBatch );
            }
            else if( mHud.GameOver )
            {
                mGameOver.Draw( _spriteBatch );
            }

            _spriteBatch.Draw( mPauseButton, mPausePosition, null, Color.White, 0f, mPauseOrigin, 1, SpriteEffects.None, 1 );


            base.Draw( _spriteBatch );
        }
    }
}
