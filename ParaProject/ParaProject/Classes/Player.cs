using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace ParaProject.Classes
{
    enum weaponType
    {
        basic,
	    dual,
	    spray
    }

    class Player : Character
    {
        Vector2 mScreenSize;
    
        int mCurrentBullet;
        int mFireRateTimer;
    
        Joystick mMvJoystick;   // Move Joystick
        Joystick mRtJoystick;   // Rotate Joystick
        
        public List<Bullet> Bullets  {get; private set;}

        int mScore;
    
        weaponType weapon;

        public Player( Texture2D _loadedtexture, Texture2D _bulletTexture, Landscape _env, Vector2 _screenSize )
        {
            mScreenSize = _screenSize;

            landscape = _env;

            sprite = _loadedtexture;
            position = Vector2.Divide( mScreenSize, 2 );

            rotation = 0;

            origin = new Vector2( sprite.Width / 2, sprite.Height / 2 );

            life = 3;

            Bullets = new List<Bullet>();
            for( int i = 0; i < 10; i++ )
            {
                Bullets.Add( new Bullet( _bulletTexture, _env ) );
            }
            mCurrentBullet = 0;
            mFireRateTimer = 0;

        }

        public void LinkJoysticks( Joystick _mvJoystick, Joystick _rtJoystick )
        {
            mMvJoystick = _mvJoystick;
            mRtJoystick = _rtJoystick;
        }

        public void IncreaseScore( int _score )
        {
            mScore += _score;
        }

        public int GetScore()
        {
            return mScore;
        }

        public void RemoveLife()
        {
            life--;
        }

        public int GetLife()
        {
            return life;
        }

        public Vector2 GetWorldPosition()
        {
            worldPosition = new Vector2( - landscape.Position.X + position.X + 1024 / 2, - landscape.Position.Y + position.Y + 1024 / 2 );
            
            return worldPosition;
        }

        

        public void UpdatePosition()
        {

            if( mMvJoystick.Offset != 0 )
            {
        
                float power = mMvJoystick.Offset / 10;
        
                landscape.Position = Vector2.Add( landscape.Position, new Vector2( (float) Math.Cos( mMvJoystick.Angle + Math.PI ) * power, (float) Math.Sin( mMvJoystick.Angle + Math.PI ) * power ) ); 
                
                if( landscape.Position.X > ( 1024 / 2 ) || position.X < mScreenSize.X / 2  )
                {
                    landscape.Position = new Vector2( ( 1024 / 2 ), landscape.Position.Y );
                    
            
                    float newX = Math.Min( position.X - (float) Math.Cos( mMvJoystick.Angle + Math.PI ) * power, mScreenSize.X / 2 );
                        
                    position = new Vector2( newX, position.Y );
            
                    if( position.X < 40 )
                        position = new Vector2( 40, position.Y );

                }
                if( landscape.Position.X < ( - 1024 / 2 + (mScreenSize.X / 2) * 2 ) || position.X > mScreenSize.X / 2)
                {
                    landscape.Position = new Vector2( -( 1024 / 2 ) + mScreenSize.X / 2 * 2 , landscape.Position.Y );
                    
                    float newX = Math.Max( position.X - (float) Math.Cos( mMvJoystick.Angle + Math.PI ) * power, mScreenSize.X / 2 );

                    position = new Vector2( newX, position.Y );
            
                    if( position.X > mScreenSize.X - 40 )
                        position = new Vector2( mScreenSize.X - 40, position.Y );
            
                }
                if( landscape.Position.Y > ( 1024 / 2  ) || position.Y < mScreenSize.Y / 2 )
                {
                    landscape.Position = new Vector2( landscape.Position.X, ( 1024 / 2 ) );
                    
                    float newY = Math.Min( position.Y - (float) Math.Sin( mMvJoystick.Angle + Math.PI ) * power, mScreenSize.Y / 2 );

                    position = new Vector2( position.X, newY);
            
                    if( position.Y < 40 )
                        position = new Vector2( position.X, 40);
                
                }
                if( landscape.Position.Y < ( - 1024 / 2 + (mScreenSize.Y / 2) * 2 ) || position.Y > mScreenSize.Y / 2  )
                {
                    landscape.Position = new Vector2( landscape.Position.X, - 1024 / 2 + (mScreenSize.Y / 2) * 2 );


                    float newY = Math.Max( position.Y - (float) Math.Sin( mMvJoystick.Angle + Math.PI ) * power, mScreenSize.Y / 2 );

                    position = new Vector2( position.X, newY);
            
                    if( position.Y > mScreenSize.Y - 40 )
                        position = new Vector2( position.X, mScreenSize.Y - 40);
                }
        
                if( mRtJoystick.Offset == 0 )
                {
                    rotation = (float)( mMvJoystick.Angle - Math.PI / 2 );
                }

                
        
            }

            if( mRtJoystick.Offset != 0 )
            {
                rotation = (float)( mRtJoystick.Angle - Math.PI / 2 );
                if( mFireRateTimer > 10 )
                {
                    Bullets[mCurrentBullet].SetSpritePosition( Vector2.Subtract( this.GetWorldPosition(), new Vector2( 1024/2 , 1024/2 ) ) );
                    Bullets[mCurrentBullet].Angle = (float) ( mRtJoystick.Angle - Math.PI );

        
                    if( mCurrentBullet < Bullets.Count() - 1 )
                        mCurrentBullet++;
                    else
                        mCurrentBullet = 0;
            
                    mFireRateTimer = 0;
                }
                else
                    mFireRateTimer++;
            }
            
            foreach( Bullet bullet in Bullets )
            {
                if( bullet.Angle != 0 )
                {
                    bullet.SetSpritePosition( Vector2.Subtract( bullet.GetSpritePosition(), new Vector2( (float) ( Math.Cos( bullet.Angle ) * 5 ), (float) ( Math.Sin( bullet.Angle ) * 5 ) ) ) );
                }
            }
        }

        public override void Draw( SpriteBatch _spriteBatch )
        {
            foreach( Bullet bullet in Bullets )
            {
                bullet.Draw( _spriteBatch );
            }
            
            _spriteBatch.Draw( sprite, position, null, Color.White, rotation, origin, 1, SpriteEffects.None, 1 );

            
        }
    }
}
